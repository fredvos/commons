/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.cli;

import java.util.*;

public class CommandDefinition {

  public String longCommand;
  public String shortCommand;
  public List<String> requiredArguments;
  public List<String> optionalArguments;

  public CommandDefinition(String longCommand, String shortCommand) {
    this.longCommand = longCommand;
    this.shortCommand = shortCommand;
    this.requiredArguments = new ArrayList<String>();
    this.optionalArguments = new ArrayList<String>();
  }

  public CommandDefinition addRequiredArgument(String argumentName) {
    this.requiredArguments.add(argumentName);
    return this;
  }

  public CommandDefinition addOptionalArgument(String argumentName) {
    this.optionalArguments.add(argumentName);
    return this;
  }

}
