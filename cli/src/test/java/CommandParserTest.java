/*
 * Copyright Ⓒ 2018 Fred Vos
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mokolo.commons.cli.CommandDefinition;
import org.mokolo.commons.cli.CommandParser;
import org.mokolo.commons.cli.ParsingException;
import org.mokolo.commons.data.Document;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class CommandParserTest {

  private CommandParser commandParser;
  
  @BeforeEach
  public void init() {
    this.commandParser = new CommandParser(new OptionParser(), "request");
    this.commandParser.addCommandDefinition(
        new CommandDefinition("do-this", "this")
            .addRequiredArgument("with-file")
            .addRequiredArgument("output-file"));
    this.commandParser.addCommandDefinition(
        new CommandDefinition("do-that", "that")
            .addRequiredArgument("with-file")
            .addRequiredArgument("output-file")
            .addOptionalArgument("opt-1")
            .addOptionalArgument("opt-2"));
    this.commandParser.addCommandDefinition(
        new CommandDefinition("multi", null)
            .addRequiredArgument("file"));
  }

  @Test
  public void canParseCommandLineAndGetValues() throws ParsingException {
    String[] args = { "do-this", "source1", "destination1" };
    Document document = this.commandParser.parseCommandNoSwitches(args);
    this.commandParser.validateDocument(document); // Must create valid document
    assertEquals("source1", document.getString("with-file"), "First argument value ok");
    assertEquals("destination1", document.getString("output-file"), "Second argument value ok");
  }
  
  @Test
  public void commandLineWithTooManyArgumentsThrowsException()
  throws ParsingException {
    String[] args = { "do-this", "source1", "destination1", "bla" };
    assertThrows(ParsingException.class, () -> {
      this.commandParser.parseCommandNoSwitches(args);
    });
  }
  
  @Test
  public void canParseCommandLineWith0OptionalArgumentsAndGetValues()
  throws ParsingException {
    String[] args = { "do-that", "source1", "destination1" };
    Document document = this.commandParser.parseCommandNoSwitches(args);
    commandParser.validateDocument(document); // Must create valid document
    assertEquals("source1", document.getString("with-file"), "First argument value ok");
    assertEquals("destination1", document.getString("output-file"), "Second argument value ok");
    assertNull(document.getString("opt-1"), "Optional argument null");
  }
  
  @Test
  public void canParseCommandLineWithAlternativeCommandAndWith1OptionalArgumentAndGetValues()
  throws ParsingException {
    String[] args = { "that", "source1", "destination1", "bla" };
    Document document = this.commandParser.parseCommandNoSwitches(args);
    commandParser.validateDocument(document); // Must create valid document
    assertEquals("do-that", document.getString("request"), "Command");
    assertEquals("bla", document.getString("opt-1"), "Optional argument value ok");
    assertNull(document.getString("opt-2"), "Second optional argument null");
  }
  
  @Test
  public void canParseCommandLineWithAlternativeCommandAndSwitchesAndGetValues()
  throws ParsingException {
    String[] args = { "that", "--with-file=source1", "--output-file=destination1", "--opt-2=foo" };
    OptionSet optionSet = this.commandParser.getOptionParser().parse(args);
    Document document = this.commandParser.parseCommandWithSwitches(optionSet);
    commandParser.validateDocument(document); // Must create valid document
    assertEquals("do-that", document.getString("request"), "Command");
    assertEquals("source1", document.getString("with-file"), "First argument value ok");
    assertEquals("destination1", document.getString("output-file"), "Second argument value ok");
    assertNull(document.getString("opt-1"), "First optional argument null");
    assertEquals("foo", document.getString("opt-2"), "Second optional argument value ok");
  }
  
  @Test
  public void canParseCommandLineWithMultipleArguments()
  throws ParsingException {
    String[] args = { "multi", "--file=source1", "--file=source2" };
    OptionSet optionSet = this.commandParser.getOptionParser().parse(args);
    Document document = this.commandParser.parseCommandWithSwitches(optionSet);
    commandParser.validateDocument(document); // Must create valid document
    assertEquals("multi", document.getString("request"), "Command");
    assertEquals(2, document.getValueCount("file"), "Two occurrences of --file=");
    assertEquals("source1", document.getStrings("file").get(0));
    assertEquals("source2", document.getStrings("file").get(1));
  }
  
}
