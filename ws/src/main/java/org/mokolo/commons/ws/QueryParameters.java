/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents list of query parameters
 * 
 * Multiple values per one key allowed.
 *
 */
public class QueryParameters extends TreeMap<String, ArrayList<String>> {
  private static final long serialVersionUID = -1493650303624924384L;

  public void add(String key, String value) {
    ArrayList<String> values = this.get(key);
    if (values == null) {
      values = new ArrayList<>();
      this.put(key,  values);
    }
    values.add(value);
  }
  
  /**
   * Returns query parameters as part of an URL
   * 
   * Parameters are sorted by key.
   * Values are sorted by adding order.
   * 
   * Example:
   * <code>
   * color=black&size=big
   * </code>
   */
  @Override
  public String toString() {
    List<String> keyValues = new ArrayList<>();
    for (Map.Entry<String, ArrayList<String>> entry : this.entrySet())
      for (String value : entry.getValue())
        keyValues.add(entry.getKey()+"="+value);
    return String.join("&", keyValues);
  }
  
  /**
   * Returns query parameters as list
   * 
   * Parameters are sorted by key.
   * 
   * Example:
   * <code>
   * - color: black
   * - size: big
   * </code>
   * 
   * Example:
   * <code>
   * - id:
   *   - 5
   *   - 1
   * - size: 20
   * </code>
   */
  public String asList() {
    StringBuffer buf = new StringBuffer();
    for (Map.Entry<String, ArrayList<String>> entry : this.entrySet()) {
      if (entry.getValue().size() <= 1)
        buf.append("- "+entry.getKey()+": "+entry.getValue().get(0)+"\n");
      else {
        buf.append("- "+entry.getKey()+":\n");
        for (String value : entry.getValue())
          buf.append("  - "+value+"\n");
      }
    }
    return buf.toString();
  }

  
}
