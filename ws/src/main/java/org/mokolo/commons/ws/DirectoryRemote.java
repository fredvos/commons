/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;

import lombok.Cleanup;

/**
 * Implementation of {@Link Remote} reading responses from a directory
 * 
 * Converts the {@Link Request} into an MD5 hash using method {@Link Request.getMd5Hash()}.
 * It then tries to open file \<hash\>.response in the directory and returns its contents as an {@Link InputStream}.
 * 
 * Caller is responsible for closing the {@Link InputStream}.
 *
 */
public class DirectoryRemote implements Remote {
  
  private File directory;
  
  public DirectoryRemote(File directory) {
    this.directory = directory;
  }
  
  public void writeToFile(Request request) throws IOException {
    String md5Hash = request.getMd5Hash();
    File requestFile = getRequestFile(md5Hash);
    FileUtils.write(requestFile, request.toLongString(), "UTF-8");    
  }

  public void writeToFiles(Response response, String basename, boolean overwrite) throws IOException {
    File responseMetadataFile = getResponseMetadataFile(basename);
    File responseDataFile = getResponseDataFile(basename);
    if (overwrite == false && (responseMetadataFile.exists() || responseDataFile.exists()))
      throw new IOException(
          "One or more files "+responseDataFile.getAbsolutePath() +
          " or " +
          responseMetadataFile.getAbsolutePath() +
          " exists and overwrite is false");
    FileOutputStream os = new FileOutputStream(responseMetadataFile);
    response.getMetadataAsProperties().store(os, "Response Metadata");
    os.close();
    FileUtils.copyInputStreamToFile(response.getInputStream(), responseDataFile);
  }
  
  public Response readFromFiles(String basename) throws IOException {
    Status status = Status.OK;
    MediaType mediaType = null;
    InputStream inputStream = null;
    File responseMetadataFile = getResponseMetadataFile(basename);
    if (! responseMetadataFile.exists())
      status = Status.NOT_FOUND;
    else {
      Properties properties = new Properties();
      @Cleanup InputStream metadataInputStream = new FileInputStream(responseMetadataFile);
      properties.load(metadataInputStream);
      status = Status.valueOf(properties.getProperty("Status"));
      mediaType = MediaType.valueOf(properties.getProperty("Content-Type"));
      File responseDataFile = getResponseDataFile(basename);
      inputStream = new FileInputStream(responseDataFile);
    }
    return new Response(status, mediaType, inputStream);
  }

  private File getRequestFile(String basename) {
    return new File(this.directory, basename+".request");
  }
  
  private File getResponseMetadataFile(String basename) {
    return new File(this.directory, basename+".response-metadata");
  }
  
  private File getResponseDataFile(String basename) {
    return new File(this.directory, basename+".response-data");
  }

  public boolean hasFiles(String basename) {
    return (
        this.getRequestFile(basename).exists() &&
        this.getResponseMetadataFile(basename).exists() &&
        this.getResponseDataFile(basename).exists());
  }
  
  @Override
  public Response call(Request request) throws IOException {
    return this.readFromFiles(request.getMd5Hash());
  }

}
