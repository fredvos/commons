package org.mokolo.commons.ws;

public enum Scheme {
  HTTP, HTTPS
}
