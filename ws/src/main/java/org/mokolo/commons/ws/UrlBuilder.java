package org.mokolo.commons.ws;

import java.io.IOException;

import java.net.URL;

/**
 * Builds URL
 * 
 * Usage:
 * 
 * URL url = new UrlBuilder()
 *     .withScheme(Scheme.HTTP)
 *     .withHost("my.remote.host")
 *     .withPort(8080)
 *     .build();
 *
 */
public class UrlBuilder implements Cloneable {

  private Scheme scheme;
  private String host;
  private Integer port;
  private String path;
  private QueryParameters queryParameters;
  
  /*
   * Initialize; default is http://localhost
   */
  public UrlBuilder() {
    this.scheme = Scheme.HTTP;
    this.host = "localhost";
    this.port = null;
    this.path = null;
    this.queryParameters = new QueryParameters();
  }
  
  @Override
  public UrlBuilder clone() {
    UrlBuilder copy = new UrlBuilder();
    copy.scheme = this.scheme;
    copy.host = this.host;
    copy.port = this.port;
    copy.path = this.path;
    copy.queryParameters.putAll(this.queryParameters);
    return copy;
  }
  
  public UrlBuilder withScheme(Scheme scheme) {
    UrlBuilder copy = this.clone();
    copy.scheme = scheme;
    return copy;
  }
  
  public UrlBuilder withHost(String host) {
    UrlBuilder copy = this.clone();
    copy.host = host;
    return copy;
  }

  public UrlBuilder withPort(Integer port) {
    UrlBuilder copy = this.clone();
    copy.port = port;
    return copy;
  }

  public UrlBuilder withPath(String path) {
    UrlBuilder copy = this.clone();
    copy.path = path;
    return copy;
  }

  public UrlBuilder withRequestParameter(String key, String value) {
    UrlBuilder copy = this.clone();
    copy.queryParameters.add(key, value);
    return copy;
  }

  public URL build() throws IOException {
    StringBuffer buf = new StringBuffer();
    if (this.scheme == null)
      throw new IOException("No scheme set in UrlBuilder");
    else
      buf.append(this.scheme.toString().toLowerCase()+"://");
    if (this.host == null)
      throw new IOException("No host set in UrlBuilder");
    else
      buf.append(this.host);
    if (this.port != null)
      buf.append(":"+this.port);
    if (this.path != null)
      buf.append("/"+this.path);
    if (! this.queryParameters.isEmpty()) {
      buf.append("?"+this.queryParameters.toString());
    }
    return new URL(buf.toString());
  }
  
}
