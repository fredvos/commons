/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.ws.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.HttpMethod;
import org.mokolo.commons.ws.Request;

@Slf4j
public class RequestTest {

  @Test
  public void doesConstructNeatText() throws Exception {
    Request r = new Request();
    r.setMethod(HttpMethod.GET);
    r.setAccept(MediaType.APPLICATION_XML_TYPE);
    r.setPath("horses");
    r.putExtraHttpRequestHeader("Foo", "bar");
    r.addQueryParameter("tail", "long");
    r.addQueryParameter("color", "white");
    r.validate();
    
    String fullText = r.toLongString();
    log.debug("Request =\n"+fullText);
    assertTrue(fullText.contains("Method: GET"), "Has method");
    assertTrue(fullText.contains("- color: white") && fullText.contains("- tail: long"), "Has query params");
  }
}
