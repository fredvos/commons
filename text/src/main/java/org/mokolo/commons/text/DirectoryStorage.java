package org.mokolo.commons.text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.mokolo.commons.data.DException;

import lombok.Cleanup;

public class DirectoryStorage extends MarkupTextStorage {
  
  private File baseDirectory;
  private MarkupFormat format;
  private MarkupAPI api;
  
  
  public DirectoryStorage(
      File baseDirectory,
      MarkupAPI api) {
    this.baseDirectory = baseDirectory;
    this.api = api;
    this.format = api.getMarkupFormat();
  }

  @Override
  public MarkupText get(String path) {
    return null;
  }
  
  @Override
  public void put(String path, MarkupText markupText)
  throws DException, IOException {
    File file = this.findOrCreateFile(path);
    @Cleanup OutputStream os = new FileOutputStream(file); 
    IOUtils.write(this.api.serialize(markupText), os, Charset.forName("UTF-8"));
  }

  /**
   * Checks all extensions for a matching file or creates one if not found
   */
  private File findOrCreateFile(String path) {
    for (String extension : this.format.getExtensions()) {
      File file = new File(
          this.baseDirectory,
          path.replaceAll("/", File.separator)+extension);
      if (file.exists())
        return file;
    }
    return new File(
        this.baseDirectory,
        path.replaceAll("/", File.separator) +
        this.format.getExtensions().get(0));
  }

}
