package org.mokolo.commons.text;

public class TextException extends Exception {
  private static final long serialVersionUID = 5141159718258825105L;

  public TextException(Exception e) {
    super(e);
  }
  
  public TextException(String msg) {
    super(msg);
  }
}
