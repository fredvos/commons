package org.mokolo.commons.text;


import java.io.File;

import org.apache.commons.lang3.NotImplementedException;
import org.mokolo.commons.data.DComment;
import org.mokolo.commons.data.DException;
import org.mokolo.commons.data.DLine;
import org.mokolo.commons.data.DList;
import org.mokolo.commons.data.DMap;
import org.mokolo.commons.data.DParagraph;
import org.mokolo.commons.data.DSimple;
import org.mokolo.commons.data.DString;
import org.mokolo.commons.data.DTable;
import org.mokolo.commons.text.MarkupTextGenerator.Level;

public class HugoPageAPI extends MarkupAPI {

  public HugoPageAPI() {
    super();
  }
  
  @Override
  public MarkupFormat getMarkupFormat() {
    return MarkupFormat.MARKDOWN;
  }

  @Override
  public boolean increaseContentStartLevelByOneIfFrontMatter() {
    return true;
  }

  @Override
  public void frontMatterPreSave(MarkupText markupText) {
    markupText.putClassType();
  }
  
  /**
   * Preferred format: YAML
   */
  @Override
  public String frontMatterToString(FrontMatter frontMatter) throws DException {
    return
        "---\n" +
        frontMatter.getDMap().toYAML() +
        "---\n";
  }

  @Override
  public String commentToString(DComment comment) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String lineToString(DLine line) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String listToString(DList list) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String mapToString(DMap map) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String paragraphToString(DParagraph paragraph) {
    TextBuffer buf = new TextBuffer();
    buf.appendString(false, 0, this.replaceLinks(paragraph.getValue()), true);
    buf.appendNewLine();
    return buf.toString();
  }

  @Override
  public String tableToString(DTable table) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String titleToString(Level level, DString text) {
    return "#".repeat(level.value) + " " + text.getValue() + "\n";
  }
  
  public String replaceLinks(String s) {
    return DSimple.linkPattern.matcher(s).replaceAll(MarkupFormat.MARKDOWN.getLinkPattern());
  }

  @Override
  public void generateHTML(MarkupText markupText, File targetFile) throws NotImplementedException {
    throw new NotImplementedException("This class has no implementation for generateHTML()");
  }

  @Override
  public String generateHTML(MarkupText markupText) throws NotImplementedException {
    throw new NotImplementedException("This class has no implementation for generateHTML()");
  }

}
