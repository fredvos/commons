/*
 * Copyright Ⓒ 2018 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mokolo.commons.xml.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.xml.parsers.DocumentBuilder;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.xml.DocumentBuilderFactory;
import org.mokolo.commons.xml.Serializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SerializerTest {

  @Test
  public void canSerializeAnElementWithContent() throws Exception {
    DocumentBuilder builder = DocumentBuilderFactory.getDocumentBuilder();
    Document doc = builder.newDocument();
    Element a = doc.createElement("a");
    a.setAttribute("href", "http://test.com");
    a.appendChild(doc.createTextNode("here"));
    
    Serializer serializer = new Serializer();
    serializer.omitXMLDeclaration();
    
    assertEquals("<a href=\"http://test.com\">here</a>", serializer.toXML(a), "HTML");
  }
  
}
