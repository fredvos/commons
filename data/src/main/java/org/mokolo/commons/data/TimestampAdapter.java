/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.mokolo.commons.lang.DateFormat;

public class TimestampAdapter extends XmlAdapter<String, Date> {

  @Override
  public String marshal(Date v) throws Exception {
    if (v != null)
      return DateFormat.DATETIME_MS_TZ.getSimpleDateFormat().format(v);
    else
      return "null";
  }

  @Override
  public Date unmarshal(String v) throws Exception {
    if (! v.equals("null"))
      return DateFormat.DATETIME_MS_TZ.getSimpleDateFormat().parse(v);
    else
      return null;
  }

}
