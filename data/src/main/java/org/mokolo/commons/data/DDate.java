package org.mokolo.commons.data;

import java.io.InputStream;
import java.util.Date;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.mokolo.commons.lang.DateFormat;

import lombok.Setter;

@XmlRootElement
public class DDate extends DAtom {
  @Setter private Date value;
  
  /* For tests */
  public static DDate parseXml(InputStream is) throws JAXBException {
    return (DDate) createUnmarshaller().unmarshal(is);
  }

  public DDate() {
    super();
  }
  
  public DDate(Date value) {
    this();
    this.value = value;
  }
  
  public DDate(Object o) {
    this();
    this.value = (Date) o;
  }
  
  @XmlElement
  @XmlJavaTypeAdapter(TimestampAdapter.class)
  public Date getValue() {
    return this.value;
  }
  
  @Override
  public String toString() {
    return DateFormat.YYYY_MM_DD.getSimpleDateFormat().format(this.value);
  }
  
  @Override
  public String toString(String format) {
    return String.format(format, this.value);
  }
  
  @Override
  public Object getObject() {
    return this.value;
  }
  
}
