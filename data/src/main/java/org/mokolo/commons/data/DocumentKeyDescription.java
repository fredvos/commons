/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.mokolo.commons.lang.DateFormat;

import lombok.Getter;

public class DocumentKeyDescription {
  public enum Type {
    BOOLEAN,   // true, false
    DATE,      // 2012-01-19
    FILE,      // /etc/app/app.conf
    FLOAT,     // 36.672
    INTEGER,   // -123, 78
    STRING,    // *
    TIMESTAMP, // 2012-01-19T14:39:00+0200
    URL,       // https://app.org/search?q=test
    UUID       // 2c4dae8c-e591-49e0-9c5a-62b310a15788
  }
  
  @Getter private String key;
  @Getter private Type type;
  @Getter private String purpose; // Default: null
  @Getter private boolean required; // Default: false
  @Getter private boolean pluralAllowed; // Default: false
  
  public DocumentKeyDescription() { }
  
  /**
   * Sets up the description.
   * 
   */
  public DocumentKeyDescription(
      String key,
      Type type) {
    this.key = key;
    this.type = type;
    this.required = false;
    this.pluralAllowed = true;
  }

  public void setKey(String key) {
    this.key = key;
  }
  
  public void setType(Type type) {
    this.type = type;
  }
  
  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }
  
  public void setRequired(boolean required) {
    this.required = required;
  }
  
  public void setPluralAllowed(boolean pluralAllowed) {
    this.pluralAllowed = pluralAllowed;
  }
  
  public void validate(List<String> values) throws ValidationException {
    if (values == null || values.size() == 0) {
      if (this.required == true)
        throw new ValidationException("Missing required value for key '"+this.key+"'");
    }
    else {
      if (values.size() > 1 && this.pluralAllowed == false)
        throw new ValidationException("Plural values not allowed for key '"+this.key+"'");
      for (String value : values) {
        try {
          switch (this.type) {
          case BOOLEAN   : validateBoolean(value);   break;
          case DATE      : validateDate(value);      break;
          case FILE      : validateFile(value);      break;
          case FLOAT     : validateFloat(value);     break;
          case INTEGER   : validateInteger(value);   break;
          case TIMESTAMP : validateTimestamp(value); break;
          case URL       : validateUrl(value);       break;
          case UUID      : validateUuid(value);      break;
          default        : validateString(value);    break;
          }
        } catch (ValidationException e) {
          throw new ValidationException("Value '"+value+"' is not valid for key '"+this.key+"'. Msg="+e.getMessage());
        }
      }
    }
  }
  
  private static void validateBoolean(String value) throws ValidationException {
    if (! value.equals("true") &&
        ! value.equals("false"))
      throw new ValidationException("only 'true' and 'false' allowed");
  }

  private static void validateDate(String value) throws ValidationException {
    try {
      /*
       * Parsing a date like '2012-01-19Z' does not throw a ParseException,
       * so we check for the length first:
       */
      if (value.length() != 10)
        throw new ParseException("", 0); // Gets caught later
      /*
       * Now we parse it.
       */
      DateFormat.YYYY_MM_DD.getSimpleDateFormat(Locale.getDefault()).parse(value);
    } catch (ParseException e) {
      throw new ValidationException("date format must be YYYY-MM-DD");
    }
  }

  private static void validateFile(String value) throws ValidationException {
    // No checks possible
  }

  private static void validateFloat(String value) throws ValidationException {
    try {
      Float.parseFloat(value);
    } catch (NumberFormatException e) {
      throw new ValidationException("value is not a float");
    }
  }

  private static void validateInteger(String value) throws ValidationException {
    try {
      Integer.parseInt(value);
    } catch (NumberFormatException e) {
      throw new ValidationException("value is not an integer");
    }
  }

  private static void validateString(String value) throws ValidationException {
    // Always okay
  }

  private static void validateTimestamp(String value) throws ValidationException {
    try {
      DateFormat.ISO_TZ.getSimpleDateFormat(Locale.UK).parse(value);
    } catch (ParseException e) {
      throw new ValidationException("timestamp format must be YYYY-MM-DDTHH:MM:SS+0000");
    }
  }

  private static void validateUrl(String value) throws ValidationException {
    try {
      new URL(value);
    } catch (MalformedURLException e) {
      throw new ValidationException("value is not a URL");
    }
  }

  private static void validateUuid(String value) throws ValidationException {
    try {
      UUID.fromString(value);
    } catch (IllegalArgumentException e) {
      throw new ValidationException("value is not a UUID");
    }
  }

}
