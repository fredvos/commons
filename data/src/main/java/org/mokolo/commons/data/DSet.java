package org.mokolo.commons.data;

import java.io.InputStream;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Setter;

@XmlRootElement
public class DSet extends DCollection {
  
  @Setter private LinkedHashSet<DObject> set;
  
  public static DSet parseXml(InputStream is) throws JAXBException {
    return (DSet) createUnmarshaller().unmarshal(is);
  }

  public DSet() {
    super();
    this.set = new LinkedHashSet<>();
  }
  
  public DSet(Object object) {
    this();
    @SuppressWarnings("unchecked")
    Set<Object> s = (Set<Object>) object;
    for (Object o : s)
      this.add((DSimple) DObject.toDObject(o));
  }

  @XmlElementWrapper(name="set")
  @XmlElement(name="item")
  public LinkedHashSet<DObject> getSet() {
    return this.set;
  }
  
  public void add(DObject value) {
    this.set.add(value);
  }
  
  public boolean contains(DObject o) {
    return this.set.contains(o);
  }

  @Override
  public Object getObject() {
    Set<Object> object = new HashSet<>();
    for (DObject item : this.set) {
      object.add(item.getObject());
    }
    return object;
  }

}
