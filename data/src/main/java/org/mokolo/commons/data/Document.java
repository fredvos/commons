/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.data;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

import org.mokolo.commons.lang.DateFormat;

public class Document extends HashMap<String, ArrayList<String>> implements Cloneable {
  private static final long serialVersionUID = 4210626571135728267L;

  @Getter @Setter private UUID uuid;
  
  public Document() {
    super();
    this.uuid = UUID.randomUUID();
  }
  
  public Document clone() {
    Document theClone = (Document) super.clone();
    theClone.uuid = UUID.randomUUID(); // new UUID
    return theClone;
  }
  
  public static void copyValues(Document source, Document destination) {
    Iterator<Map.Entry<String, ArrayList<String>>> entries = source.entrySet().iterator();
    while (entries.hasNext()) {
      Map.Entry<String, ArrayList<String>> entry = entries.next();
      ArrayList<String> values = new ArrayList<String>();
      for (String value : entry.getValue())
        values.add(value);
      destination.put(entry.getKey(), values);
    }
  }
  
  /**
   * Adds value for a key.
   * 
   * <p>If key does not exist it is created.</p>
   * 
   */
  public void addString(String key, String value) {
    ArrayList<String> values = this.get(key);
    if (values == null) {
      values = new ArrayList<String>();
      this.put(key, values);
    }
    values.add(value);
  }
  
  public void addBoolean(String key, Boolean b) {
    this.addString(key, b.toString());    
  }
  
  public void addDate(String key, Date date) {
    SimpleDateFormat sdf = DateFormat.YYYY_MM_DD.getSimpleDateFormat();
    this.addString(key, sdf.format(date));    
  }
  
  public void addFile(String key, File file) {
    this.addString(key, file.getAbsolutePath());    
  }
  
  public void addFloat(String key, Float f) {
    this.addString(key, f.toString());    
  }
  
  public void addInteger(String key, Integer integer) {
    this.addString(key, integer.toString());    
  }
  
  public void addTimestamp(String key, Date date) {
    SimpleDateFormat sdf = DateFormat.ISO_MS_TZ.getSimpleDateFormat();
    this.addString(key, sdf.format(date));    
  }
  
  public void addUrl(String key, URL url) {
    this.addString(key, url.toExternalForm());    
  }
  
  public void addUuid(String key, UUID uuid) {
    this.addString(key, uuid.toString());    
  }
  
  public int getValueCount(String key) {
    List<String> values = this.get(key);
    if (values == null)
      return 0;
    else
      return values.size();
  }
  
  /**
   * Returns first value
   * 
   * @param key
   * @return the first value if the document has at least
   *         one value for this key or null if it has none 
   */
  public String getString(String key) {
    return this.getStringValue(key, null);
  }
  
  /**
   * Returns first value of default if not found
   * 
   * @param key
   * @param defaultValue
   * @return the first value if the document has at least
   *         one value for this key or null if it has none 
   */
  public String getString(String key, String defaultValue) {
    return getStringValue(key, defaultValue);
  }

  /**
   * Returns first value of default if not found
   * 
   * @param key
   * @param defaultValue
   * @return the first value if the document has at least
   *         one value for this key or null if it has none
   *         
   * Useful for derived classes.
   */
  public String getStringValue(String key, String defaultValue) {
    List<String> values = this.getStrings(key);
    if (values != null && values.size() > 0)
      return values.get(0);
    else
      return defaultValue;
  }

  public List<String> getStrings(String key) {
    return this.get(key);
  }
  
  public Boolean getBoolean(String key, Boolean defaultValue) {
    String value = this.getString(key);
    return value != null ? Boolean.parseBoolean(value) : defaultValue;
  }

  public List<Boolean> getBooleans(String key) {
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<Boolean> list = new ArrayList<>();
      for (String value : values)
        list.add(Boolean.parseBoolean(value));
      return list;
    }
    else
      return null;
  }

  public Date getDate(String key) throws ParseException {
    String value = this.getString(key);
    if (value == null)
      return null;
    SimpleDateFormat sdf = DateFormat.YYYY_MM_DD.getSimpleDateFormat();
    return sdf.parse(value);
  }

  public List<Date> getDates(String key) throws ParseException {
    SimpleDateFormat sdf = DateFormat.YYYY_MM_DD.getSimpleDateFormat();
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<Date> list = new ArrayList<>();
      for (String value : values)
        list.add(sdf.parse(value));
      return list;
    }
    else
      return null;
  }
  
  public File getFile(String key) {
    String value = this.getString(key);
    if (value == null)
      return null;
    return new File(value);
  }

  public List<File> getFiles(String key) {
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<File> list = new ArrayList<>();
      for (String value : values)
        list.add(new File(value));
      return list;
    }
    else
      return null;
  }
  
  public Float getFloat(String key) {
    String value = this.getString(key);
    if (value == null)
      return null;
    return Float.parseFloat(value);
  }

  public List<Float> getFloats(String key) {
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<Float> list = new ArrayList<>();
      for (String value : values)
        list.add(Float.parseFloat(value));
      return list;
    }
    else
      return null;
  }

  public Integer getInteger(String key) {
    return this.getInteger(key, null);
  }

  public Integer getInteger(String key, Integer defaultValue) {
    String value = this.getString(key);
    if (value == null)
      return defaultValue;
    return Integer.parseInt(value);
  }

  public List<Integer> getIntegers(String key) {
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<Integer> list = new ArrayList<>();
      for (String value : values)
        list.add(Integer.parseInt(value));
      return list;
    }
    else
      return null;
  }

  public Date getTimestamp(String key) throws ParseException {
    List<Date> values = this.getTimestamps(key);
    if (values != null && values.size() > 0)
      return values.get(0);
    else
      return null;
  }

  public List<Date> getTimestamps(String key) throws ParseException {
    SimpleDateFormat sdf = DateFormat.ISO_MS_TZ.getSimpleDateFormat();
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<Date> list = new ArrayList<>();
      for (String value : values)
        list.add(sdf.parse(value));
      return list;
    }
    else
      return null;
  }
  
  public URL getUrl(String key) throws MalformedURLException {
    List<URL> values = this.getUrls(key);
    if (values != null && values.size() > 0)
      return values.get(0);
    else
      return null;
  }

  public List<URL> getUrls(String key) throws MalformedURLException {
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<URL> list = new ArrayList<>();
      for (String value : values)
        list.add(new URL(value));
      return list;
    }
    else
      return null;
  }
  
  public UUID getUuid(String key) {
    List<UUID> values = this.getUuids(key);
    if (values != null && values.size() > 0)
      return values.get(0);
    else
      return null;
  }

  public List<UUID> getUuids(String key) {
    List<String> values = this.getStrings(key);
    if (values != null) {
      List<UUID> list = new ArrayList<>();
      for (String value : values)
        list.add(UUID.fromString(value));
      return list;
    }
    else
      return null;
  }
  
}

