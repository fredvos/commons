package org.mokolo.commons.pckg.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.pckg.Jar;

public class JarTest {

  @Test
  public void canGenerateJar() {
    Jar jar;
    
    jar = Jar.getJar("/usr/share/java/rome.jar");
    assertEquals("/usr/share/java/rome.jar", jar.path);
    assertNull(jar.version);
    
    jar = Jar.getJar("/usr/share/java/logback-access.jar");
    assertEquals("/usr/share/java/logback-access.jar", jar.path);
    assertNull(jar.version);
    
    jar = Jar.getJar("/usr/share/java/rome-1.12.2.jar");
    assertEquals("/usr/share/java/rome.jar", jar.path);
    assertEquals("1.12.2", jar.version);

    jar = Jar.getJar("/usr/share/java/logback-access-1.2.3.jar");
    assertEquals("/usr/share/java/logback-access.jar", jar.path);
    assertEquals("1.2.3", jar.version);
  }
  
  @Test
  public void canExtractJarsFromFilesList() throws Exception {
    final List<String> fileNames = Collections.unmodifiableList(Arrays.asList(
        "/usr/share/java/logback-access-1.2.3.jar",
        "/usr/share/java/logback-access.jar", // Typical set of 2 in Debian packages
        "/usr/share/java/logback-classic-1.2.3.jar",
        "/usr/share/java/logback-classic-1.2.jar",
        "/usr/share/java/logback-classic-1.jar",
        "/usr/share/java/logback-classic.jar", // With intermediate versions
        "/usr/share/java/logback-core-1.2.3.jar" // Single file with version, non-typical
        ));
    
    List<Jar> jars = Jar.getJarsListSinglePackage(fileNames);
    
    Map<String, String> fv = new HashMap<>();
    for (Jar jar : jars)
      fv.put(jar.path, jar.version);
    
    assertEquals(3, fv.size());
    
    assertTrue(fv.containsKey("/usr/share/java/logback-access.jar"));
    assertEquals("1.2.3", fv.get("/usr/share/java/logback-access.jar"));
    
    assertTrue(fv.containsKey("/usr/share/java/logback-classic.jar"));
    assertEquals("1.2.3", fv.get("/usr/share/java/logback-classic.jar"));

    assertTrue(fv.containsKey("/usr/share/java/logback-core-1.2.3.jar"));
    assertEquals("1.2.3", fv.get("/usr/share/java/logback-core-1.2.3.jar"));
  }
  
  @Test
  public void canHandleRomeProblem() throws Exception {
    final List<String> fileNames = Collections.unmodifiableList(Arrays.asList(
        "/usr/share/java/rome.jar",
        "/usr/share/java/rome-1.12.2.jar",
        "/usr/share/java/rome-utils-1.12.2.jar",
        "/usr/share/java/rome-utils.jar"
        ));
    
    List<Jar> jars = Jar.getJarsListSinglePackage(fileNames);
    
    Map<String, String> fv = new HashMap<>();
    for (Jar jar : jars)
      fv.put(jar.path, jar.version);
    
    assertEquals(2, fv.size());
    
    assertTrue(fv.containsKey("/usr/share/java/rome.jar"));
    assertEquals("1.12.2", fv.get("/usr/share/java/rome.jar"));
    
    assertTrue(fv.containsKey("/usr/share/java/rome-utils.jar"));
    assertEquals("1.12.2", fv.get("/usr/share/java/rome-utils.jar"));
  }
  
}
