package org.mokolo.commons.pckg;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mokolo.commons.cmd.CommandRunner;
import org.mokolo.commons.cmd.Output;

public class Release {
  public String distributorID;
  public String description;
  public String release;
  public String codename;

  private static String LSB_RELEASE_COMMAND = "/usr/bin/lsb_release";
  
  public static Release getRelease() throws IOException {
    Output output = CommandRunner.runCommand(LSB_RELEASE_COMMAND+" -a");
    String[] ignoredErrors = { "No LSB" };
    List<String> lines = CommandRunner.outputToListOfStrings(output, ignoredErrors);
    Map<String, String> kv = new HashMap<>();
    Pattern pattern = Pattern.compile("^(.*):\\s+(.*)$");
    for (String line : lines) {
      Matcher matcher = pattern.matcher(line);
      if (matcher.find())
        kv.put(matcher.group(1), matcher.group(2));
    }
    Release release = new Release();
    release.distributorID = kv.get("Distributor ID");
    release.description = kv.get("Description");
    release.release = kv.get("Release");
    release.codename = kv.get("Codename");
    return release;
  }
  
  public static boolean canTest() {
    File lsbReleaseCommandFile = new File(LSB_RELEASE_COMMAND);
    return lsbReleaseCommandFile.exists();
  }
}
