package org.mokolo.commons.pckg;

import java.util.Comparator;

public class PackageComparator implements Comparator<Package> {

  public int compare(Package a, Package b) {
    return a.name.compareTo(b.name);
  }
}
