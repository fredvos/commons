/*
 * Copyright Ⓒ 2019 Fred Vos
 * Copyright Ⓒ 2019 Tilburg University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog;

import java.io.IOException;

/**
 * Usage:
 * 
 * HttpdLogEtrySerializer s = new ImplementationOfSerializer(...);
 * s.set...();
 * s.open();
 * s.printHeader();
 * for (HttpdLogEntry entry : ...)
 *   s.printLogEntry(entry);
 * s.printFooter();
 * s.close();
 *
 */
public interface HttpdLogEntrySerializer {
  void open() throws IOException;
  void printHeader() throws IOException;
  void printLogEntry(HttpdLogEntry logEntry) throws IOException;
  void printFooter() throws IOException;
  void close() throws IOException;
}
