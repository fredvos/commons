/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mokolo.commons.io.HttpMethod;

public class HttpdLogEntryMatcherImpl implements HttpdLogEntryMatcher {

  private Set<Pattern> anyInetAddresses;
  private Set<HttpMethod> anyHttpMethods;
  private Set<Pattern> anyPaths;
  private Set<Integer> anyStatuses;
  private Set<Integer> allNotStatuses;
  
  public HttpdLogEntryMatcherImpl() {
    this.anyInetAddresses = new HashSet<>();
    this.anyHttpMethods = new HashSet<>();
    this.anyPaths = new HashSet<>();
    this.anyStatuses = new HashSet<>();
    this.allNotStatuses = new HashSet<>();
  }

  public HttpdLogEntryMatcherImpl(HttpdLogEntryMatcherImpl src) {
    this();
    this.anyInetAddresses.addAll(src.anyInetAddresses);
    this.anyHttpMethods.addAll(src.anyHttpMethods);
    this.anyPaths.addAll(src.anyPaths);
    this.anyStatuses.addAll(src.anyStatuses);
    this.allNotStatuses.addAll(src.allNotStatuses);
  }
  
  public void addInetAddress(Pattern address) {
    this.anyInetAddresses.add(address);
  }
  
  public void addHttpMethod(HttpMethod method) {
    this.anyHttpMethods.add(method);
  }
  
  public void addPath(Pattern path) {
    this.anyPaths.add(path);
  }
  
  public void addStatus(Integer status) {
    this.anyStatuses.add(status);
  }
  
  public void addNotStatus(Integer status) {
    this.allNotStatuses.add(status);
  }
  
  @Override
  public boolean matches(HttpdLogEntry entry) {
    return (
        (this.anyInetAddresses.isEmpty() || stringMatchesAny(this.anyInetAddresses, entry.getInetAddress().getHostAddress())) &&
        (this.anyHttpMethods.isEmpty() || httpMethodMatchesAny(this.anyHttpMethods, entry.getHttpMethod())) &&
        (this.anyPaths.isEmpty() || stringMatchesAny(this.anyPaths, entry.getPath())) &&
        (this.anyStatuses.isEmpty() || statusMatchesAny(this.anyStatuses, entry.getStatus())) &&
        ! statusMatchesAny(this.allNotStatuses, entry.getStatus())
        );
  }
  
  /**
   * Returns true if string matches any of the patterns
   */
  private static boolean stringMatchesAny(Set<Pattern> patterns, String s) {
    for (Pattern pattern : patterns) {
      Matcher matcher = pattern.matcher(s);
      if (matcher.find())
        return true;
    }
    return false;
  }

  /**
   * Returns true if method equals of the methods
   */
  private static boolean httpMethodMatchesAny(Set<HttpMethod> methods, HttpMethod method) {
    for (HttpMethod anyMethod : methods) {;
      if (anyMethod == method)
        return true;
    }
    return false;
  }

  /**
   * Returns true if status equals of the methods
   */
  private static boolean statusMatchesAny(Set<Integer> statuses, Integer status) {
    for (Integer anyStatus : statuses) {;
      if (anyStatus.equals(status))
        return true;
    }
    return false;
  }
}
