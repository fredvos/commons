/*
 * Copyright Ⓒ 2019 Tilburg University
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.commons.io.httpdlog.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;

import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryCSVSerializer;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryCSVSerializer.Column;
import org.mokolo.commons.io.httpdlog.HttpdLogEntryCSVSerializer.CsvFormat;
import org.mokolo.commons.lang.DateFormat;

@Slf4j
public class HttpdLogEntrySerializerTest {

  @Test
  public void canConvertLogEntriesToCSV() throws Exception {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    HttpdLogEntryCSVSerializer serializer = new HttpdLogEntryCSVSerializer(os);
    serializer.selectColumn(Column.TIMESTAMP);
    serializer.selectColumn(Column.INET_ADDRESS);
    serializer.selectColumn(Column.PATH);
    serializer.setTimestampFormat(DateFormat.DATETIME);
    serializer.setCsvFormat(CsvFormat.EXCEL);
    serializer.open();
    serializer.printHeader();
    serializer.printLogEntry(ApacheCombinedLogParserTest.getEntry1());
    serializer.printLogEntry(ApacheCombinedLogParserTest.getEntry2());
    serializer.printFooter();
    serializer.close();
    
    String s = os.toString("UTF-8");
    log.debug("CSV:\n"+s);
    
    assertTrue(s.contains("11.12.13.14"), "Found IP address");
  }
}
