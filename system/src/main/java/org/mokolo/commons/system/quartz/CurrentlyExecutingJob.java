package org.mokolo.commons.system.quartz;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.mokolo.commons.data.TimestampAdapter;

public class CurrentlyExecutingJob {
  @XmlAttribute(name = "class-name") public String className;
  @XmlAttribute(name = "fire-time") @XmlJavaTypeAdapter(TimestampAdapter.class) public Date fireTime;
  
  public Map<String, Object> toModel() {
    Map<String, Object> model = new HashMap<>();
    model.put("className", this.className);
    model.put("fireTime", this.fireTime);
    return model;
  }

}
