package org.mokolo.commons.system.quartz;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class JobGroup {
  @XmlAttribute public String name;
  @XmlElementWrapper(name = "scheduler-jobs")
  @XmlElement(name = "job")
  public List<SchedulerJob> schedulerJobs;
  
  public JobGroup() {
    this.schedulerJobs = new ArrayList<>();
  }
}
